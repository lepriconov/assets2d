using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GemController : MonoBehaviour
{
    public GameObject explosionFX;
    [HideInInspector]
    public Vector2Int posIndex;
    public Match3BoardController boardController;
    private Vector2 firstTouchPos, lastTouchPos;
    private Vector3 currentPos, moveToPos;
    private bool mousePressed, isMouseOver = false, isMarkedToDestroy = false;
    private float swipeAngle = 0, elapsedTime;
    private GemController otherGem;
    public enum GemType { blue, green, red, yellow, purple, bigBomb};
    public GemType type;
    public bool isMatched = false;
    private Vector2Int previousPos;
    [SerializeField]
    private Sprite idleSprite, mouseOverSprite;


    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<SpriteRenderer>().sprite = idleSprite;
    }

    // Update is called once per frame
    void Update()
    {
        if(currentPos != moveToPos)
        {
            elapsedTime += Time.deltaTime;
            float percentageComplete = elapsedTime / boardController.gemSpeed;
            transform.position = Vector3.Lerp(currentPos, moveToPos, boardController.gemsAnimationControl.Evaluate(percentageComplete));
            boardController.allGems[posIndex.x, posIndex.y] = this;
        }
        
        
        //захватываем последнюю позицию и вычисляем угол свайпа
        if(mousePressed && Input.GetMouseButtonUp(0))
        {
            mousePressed = false;
            if(boardController.currentState == Match3BoardController.BoardState.move)
            {
                lastTouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                CalculateAngle();
            }
            
        }

        if(isMatched && !isMarkedToDestroy)
        {
            MarkedToDestroy();
        }

        if(!isMouseOver && !isMatched)
        {
            transform.DOScale(new Vector3(boardController.gemInitialSize, 
                boardController.gemInitialSize, 
                boardController.gemInitialSize), boardController.gemSpeed);
        }
    }

    public void SetupGem(Vector2Int pos, Match3BoardController currentBoard)
    {
        posIndex = pos;
        boardController = currentBoard;
    }

    private void OnMouseDown()
    {
        if(IsBomb(this) == true)
        {
            boardController.matchFinder.CheckForBombType(this);
        }
        //захватываем первую позицию свайпа
        if(boardController.currentState == Match3BoardController.BoardState.move)
        {
            firstTouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePressed = true;
            
        }
        
    }
    

    private void OnMouseOver()
    {
        if(boardController.currentState == Match3BoardController.BoardState.move)
        {
            this.GetComponent<SpriteRenderer>().sprite = mouseOverSprite;
            isMouseOver = true;
            transform.DOScale(new Vector3(boardController.gemMouseOverSize, 
                boardController.gemMouseOverSize, 
                boardController.gemMouseOverSize), boardController.gemSpeed);
        }
        
    }

    private void OnMouseExit()
    {
        this.GetComponent<SpriteRenderer>().sprite = idleSprite;
        isMouseOver = false;
    }

    private void CalculateAngle()
    {
        // вычисляем угол свайпа
        swipeAngle = Mathf.Atan2(lastTouchPos.y - firstTouchPos.y, lastTouchPos.x - firstTouchPos.x);
        // переводим значение к градусам
        swipeAngle = swipeAngle * 180 / Mathf.PI;
        // двигаем гемы
        if(Vector3.Distance(firstTouchPos, lastTouchPos) > .5f)
        {
            MoveGems();
        }
    }

    private void MoveGems()
    {
        previousPos = posIndex;
        // меняем местами гемы на доске
        // свайп влево
        if(swipeAngle > 135 || swipeAngle < -135 && posIndex.x > 0)
        {
            StartCoroutine(SetGemBorder());
            
            currentPos = new Vector3(posIndex.x, posIndex.y, 0f);
            otherGem = boardController.allGems[posIndex.x - 1, posIndex.y];
            GetComponent<SpriteRenderer>().sortingOrder = otherGem.GetComponent<SpriteRenderer>().sortingOrder + 1;
            otherGem.currentPos = new Vector3(posIndex.x - 1, posIndex.y, 0f);
            otherGem.moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
            otherGem.posIndex.x++;
            posIndex.x--;
            moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
            elapsedTime = 0;
            otherGem.elapsedTime = 0;
            
        } else
        // свайп вправо
        if(swipeAngle < 45 && swipeAngle > -45 && posIndex.x < boardController.boardWidth - 1)
        {
            StartCoroutine(SetGemBorder());

            currentPos = new Vector3(posIndex.x, posIndex.y, 0f);
            otherGem = boardController.allGems[posIndex.x + 1, posIndex.y];
            GetComponent<SpriteRenderer>().sortingOrder = otherGem.GetComponent<SpriteRenderer>().sortingOrder + 1;
            otherGem.currentPos = new Vector3(posIndex.x + 1, posIndex.y, 0f);
            otherGem.moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
            otherGem.posIndex.x--;
            posIndex.x++;
            moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
            elapsedTime = 0;
            otherGem.elapsedTime = 0;
        } else 
        // свайп вверх
        if(swipeAngle > 45 && swipeAngle < 135 && posIndex.y < boardController.boardHeight - 1)
        {
            StartCoroutine(SetGemBorder());

            currentPos = new Vector3(posIndex.x, posIndex.y, 0f);
            otherGem = boardController.allGems[posIndex.x, posIndex.y + 1];
            GetComponent<SpriteRenderer>().sortingOrder = otherGem.GetComponent<SpriteRenderer>().sortingOrder + 1;
            otherGem.currentPos = new Vector3(posIndex.x, posIndex.y + 1, 0f);
            otherGem.moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
            otherGem.posIndex.y--;
            posIndex.y++;
            moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
            elapsedTime = 0;
            otherGem.elapsedTime = 0;
        } else
        // свайп вниз
        if(swipeAngle < -45 && swipeAngle > -135 && posIndex.y > 0)
        {
            StartCoroutine(SetGemBorder());

            currentPos = new Vector3(posIndex.x, posIndex.y, 0f);
            otherGem = boardController.allGems[posIndex.x, posIndex.y - 1];
            GetComponent<SpriteRenderer>().sortingOrder = otherGem.GetComponent<SpriteRenderer>().sortingOrder + 1;
            otherGem.currentPos = new Vector3(posIndex.x, posIndex.y - 1, 0f);
            otherGem.moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
            otherGem.posIndex.y++;
            posIndex.y--;
            moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
            elapsedTime = 0;
            otherGem.elapsedTime = 0;
        }

        // задаём новые значения для объектов
        boardController.allGems[posIndex.x, posIndex.y] = this;
        boardController.allGems[otherGem.posIndex.x, otherGem.posIndex.y] = otherGem;

        // проверяем есть ли матч и решаем, что делать дальше
        StartCoroutine(CheckMoveCo());
        
    }

    public IEnumerator CheckMoveCo()
    {
        boardController.currentState = Match3BoardController.BoardState.wait;
        yield return new WaitForSeconds(boardController.gemSpeed);
        boardController.matchFinder.FindAllMatches();
        if(otherGem != null)
        {
            
            // проверям есть ли матч
            if(!isMatched && !otherGem.isMatched)
            {
                // задаём параметры для обратного перемещения
                currentPos = new Vector3(posIndex.x, posIndex.y, 0f);
                moveToPos = new Vector3(otherGem.posIndex.x, otherGem.posIndex.y, 0f);
                otherGem.currentPos = new Vector3(otherGem.posIndex.x, otherGem.posIndex.y, 0f);
                otherGem.moveToPos = new Vector3(posIndex.x, posIndex.y, 0f);
                elapsedTime = 0;
                otherGem.elapsedTime = 0;
                // меняем расположение среди индексов
                otherGem.posIndex = posIndex;
                posIndex = previousPos;
                // меняем положение объектов
                boardController.allGems[posIndex.x, posIndex.y] = this;
                boardController.allGems[otherGem.posIndex.x, otherGem.posIndex.y] = otherGem;
                // передаём управление игроку
                yield return new WaitForSeconds(boardController.gemSpeed);
                boardController.currentState = Match3BoardController.BoardState.move;
            }
            // если матч есть - взрываем
            else
            {
                boardController.DestroyMatches();
            }
        }   
    }

    public void SetCurrentPos(Vector3 posToSet)
    {
        currentPos = posToSet;
    }

    public void SetMoveToPos(Vector3 posToSet)
    {
        moveToPos = posToSet;
        elapsedTime = 0;
    }

    private void MarkedToDestroy()
    {
        isMarkedToDestroy = true;
        //this.GetComponent<SpriteRenderer>().sprite = mouseOverSprite;
        transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), boardController.gemSpeed * 2);
        transform.DOShakeRotation(boardController.gemSpeed * 10, 5, 50, 90, false);
    }

    private IEnumerator SetGemBorder()
    {
        GameObject gemBorder = Instantiate(boardController.gemBorderPrefab, transform.position, Quaternion.identity);
        gemBorder.transform.parent = transform;
        yield return new WaitForSeconds(boardController.gemSpeed);
        Destroy(gemBorder.gameObject);
    }

    private bool IsBomb(GemController gemToCheck)
    {
        if(gemToCheck.type == GemType.bigBomb)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
