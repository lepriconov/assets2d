using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MatchFinder : MonoBehaviour
{
    private Match3BoardController board;
    public List<GemController> currentMatches = new List<GemController>();
    private void Awake()
    {
        board = FindObjectOfType<Match3BoardController>();
    }

    public void FindAllMatches()
    {
        currentMatches.Clear();
        
        for(int x = 0; x < board.boardWidth; x++)
        {
            for(int y = 0; y < board.boardHeight; y++)
            {
                GemController currentGem = board.allGems[x, y];
                if(currentGem != null)
                {
                    // ищем совпадения по X
                    if(x > 0 && x < board.boardWidth - 1)
                    {
                        GemController leftGem = board.allGems[x - 1, y];
                        GemController rightGem = board.allGems[x + 1, y];
                        if(leftGem != null && rightGem != null)
                        {
                            
                            if(leftGem.type == currentGem.type && rightGem.type == currentGem.type)
                            {  
                                // добавляем флаги
                                currentGem.isMatched = true;
                                leftGem.isMatched = true;
                                rightGem.isMatched = true;
                                // добавляем в список совпадений
                                currentMatches.Add(currentGem);
                                currentMatches.Add(leftGem);
                                currentMatches.Add(rightGem);
                            }
                        }
                    }
                    // ищем совпадения по Y
                    if(y > 0 && y < board.boardHeight - 1)
                    {
                        GemController topGem = board.allGems[x, y + 1];
                        GemController bottomGem = board.allGems[x, y - 1];
                        if(topGem != null && bottomGem != null)
                        {
                            
                            if(topGem.type == currentGem.type && bottomGem.type == currentGem.type)
                            {
                                // добавляем флаги
                                currentGem.isMatched = true;
                                topGem.isMatched = true;
                                bottomGem.isMatched = true;
                                // добавляем в список совпадений
                                currentMatches.Add(currentGem);
                                currentMatches.Add(topGem);
                                currentMatches.Add(bottomGem);
                            }
                        }
                    }
                }
            }
        }
        // удаляем из листа дубликаты
        if(currentMatches.Count > 0)
        {
            currentMatches = currentMatches.Distinct().ToList();
        }
        
    }

    public void CheckForBombType(GemController gemToCheck)
    {
        if(gemToCheck.type == GemController.GemType.bigBomb)
        {
            //Debug.Log("Нашёл большую бомбу");
            MarkBigBombArea(new Vector2Int(gemToCheck.posIndex.x, gemToCheck.posIndex.y));
        }
    }

    public void MarkBigBombArea(Vector2Int bombPos)
    {
        for(int x = bombPos.x - board.bigBombRadius; x <= bombPos.x + board.bigBombRadius; x++)
        {
            for(int y = bombPos.y - board.bigBombRadius; y <= bombPos.y + board.bigBombRadius; y++)
            {
                if(x >= 0 && x < board.boardWidth && y >= 0 && y < board.boardHeight)
                {
                    if(board.allGems[x, y] != null)
                    {
                        board.allGems[x, y].isMatched = true;
                        currentMatches.Add(board.allGems[x, y]);
                        //Debug.Log("Пометил ячейку для взрыва"+x+(":")+y);
                    }
                }
            }
        }

        currentMatches = currentMatches.Distinct().ToList();
        board.DestroyMatches();
        //Debug.Log("Отправил на уничтожение");
    }
}
