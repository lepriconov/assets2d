using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Match3BoardController : MonoBehaviour
{
    public int boardWidth, boardHeight;
    [SerializeField]
    private bool isRefillble = true;
    public GameObject tilePrefab, backgroundPrefab, matchExplosionPrefab, gemBorderPrefab;
    public GemController[] gemsToUse;
    public GemController bigBomb;
    public float bigBombChance = 2f;
    public int bigBombRadius = 2;

    public GemController[,] allGems;
    public float gemSpeed, gemInitialSize, gemMouseOverSize, gemExplosionSize;
    public AnimationCurve gemsAnimationControl;
    public MatchFinder matchFinder;
    public enum BoardState { wait, move};
    public BoardState currentState = BoardState.move;
    private Match3BoardLayout boardLayout;
    private GemController[,] layoutStore;
    [SerializeField]
    private float fieldOffset;
    
    private void Awake()
    {
        matchFinder = FindObjectOfType<MatchFinder>();
        boardLayout = GetComponent<Match3BoardLayout>();
    }
    // Start is called before the first frame update
    void Start()
    {
        allGems = new GemController[boardWidth, boardHeight];
        layoutStore = new GemController[boardWidth, boardHeight];

        Setup();
    }

    // Update is called once per frame
    void Update()
    {
        matchFinder.FindAllMatches();
    }

    private void Setup()
    {
        // спавним задник
        GameObject background = Instantiate(backgroundPrefab, new Vector2(boardWidth/2f - .5f, boardHeight/2f - .5f), Quaternion.identity);
        background.gameObject.GetComponent<SpriteRenderer>().size = new Vector2(boardWidth + fieldOffset, boardHeight + fieldOffset);
        // проверям есть ли лейаут и вызываем заполнение, если есть
        if(boardLayout != null)
        {
            layoutStore = boardLayout.GetLayout();
        }
        // создаём основное наполнение
        for(int x = 0; x < boardWidth; x++)
        {
            for(int y = 0; y < boardHeight; y++)
            {
                // создаём тайл
                Vector2 pos = new Vector2(x, y);
                GameObject tile = Instantiate(tilePrefab, pos, Quaternion.identity);
                tile.transform.parent = transform;
                tile.name = "X" + x + "Y" + y + " Tile";
                //проверям есть ли в лэйауте гем на текущую клетку и если есть, то спавним его
                if(layoutStore[x, y] != null)
                {
                    SpawnGem(new Vector2Int(x, y), layoutStore[x, y]);
                }
                else
                {
                    // выбираем гем
                    int gemIDToUse = Random.Range(0, gemsToUse.Length);
                    // проверяем, не создаёт ли матч
                    while(MatchesAt(new Vector2Int(x, y), gemsToUse[gemIDToUse]))
                    {
                        gemIDToUse = Random.Range(0, gemsToUse.Length);
                    }
                    // создаём проверенный гем
                    SpawnGem(new Vector2Int(x, y), gemsToUse[gemIDToUse]);
                }
                
                
            }
        }
    }

    private void SpawnGem(Vector2Int pos, GemController gemToSpawn)
    {
        if(Random.Range(0f, 100f) < bigBombChance)
        {
            gemToSpawn = bigBomb;
        }
        GemController gem = Instantiate(gemToSpawn, new Vector2(pos.x, pos.y + boardHeight), Quaternion.identity);
        gem.SetCurrentPos(new Vector3(pos.x, pos.y + boardHeight, 0f));
        gem.SetMoveToPos(new Vector3(pos.x, pos.y, 0f));
        gem.transform.parent = transform;
        gem.name = "X" + pos.x + "Y" + pos.y + " Gem";
        allGems[pos.x, pos.y] = gem;
        gem.SetupGem(pos, this);
        //GameObject gemFX = new GameObject();
        //gemFX.transform.parent = gem.transform;
        //gemFX.name = "gemFX";
    }

    bool MatchesAt(Vector2Int posToCheck, GemController gemToCheck)
    {
        // проверям гем на матч3 влево
        if(posToCheck.x > 1)
        {
            if(allGems[posToCheck.x - 1, posToCheck.y].type == gemToCheck.type &&
               allGems[posToCheck.x - 2, posToCheck.y].type == gemToCheck.type)
            {
                return true;
            }
        }
        // проверяем на матч3 вниз
        if(posToCheck.y > 1)
        {
            if(allGems[posToCheck.x, posToCheck.y - 1].type == gemToCheck.type &&
               allGems[posToCheck.x, posToCheck.y - 2].type == gemToCheck.type)
            {
                return true;
            }
        }
        // если матчей нет, то возвращаем добро на спавн
        return false;
    }

    private void DestroyMatchedGemAt(Vector2Int pos)
    {
        if(allGems[pos.x, pos.y] != null)
        {
            if(allGems[pos.x, pos.y].isMatched)
            {
                Instantiate(allGems[pos.x, pos.y].explosionFX, new Vector2(pos.x, pos.y), Quaternion.identity);
                Destroy(allGems[pos.x, pos.y].gameObject);
                allGems[pos.x, pos.y] = null;
            }
        }
    }

    public void DestroyMatches()
    {
        for(int i = 0; i < matchFinder.currentMatches.Count; i++)
        {
            if(matchFinder.currentMatches[i] != null)
            {
                //StartCoroutine(DestroyMatchedGemAtCo(matchFinder.currentMatches[i].posIndex));
                DestroyMatchedGemAt(matchFinder.currentMatches[i].posIndex);
            }
        }
        // опускаем гемы, если снизу есть пустоты
        StartCoroutine(DecreaseRowCo());
    }

    private IEnumerator DecreaseRowCo()
    {
        yield return new WaitForSeconds(gemSpeed);

        int nullCounter = 0;
        for(int x = 0; x < boardWidth; x++)
        {
            for(int y = 0; y < boardHeight; y++)
            {
                // пустые ячейки добавляем список
                if(allGems[x, y] == null)
                {
                    nullCounter++;
                }
                // если гем в ячейке и есть пустое место внизу - двигаем
                else if(nullCounter > 0)
                {
                    // задаём координаты для движения
                    allGems[x, y].SetCurrentPos(new Vector3(x, y, 0f));
                    allGems[x, y].SetMoveToPos(new Vector3(x, y - nullCounter, 0f));
                    // вносим изменения
                    allGems[x, y].posIndex.y -= nullCounter;
                    allGems[x, y - nullCounter] = allGems[x, y];
                    allGems[x, y] = null;
                }
            }

            nullCounter = 0;
        }
        // запускаем спавн новых гемов

        StartCoroutine(FillBoardCo());

        

    }

    private IEnumerator FillBoardCo()
    {
        currentState = BoardState.wait;
        if(isRefillble)
        {
            RefillBoard();
        }
        yield return new WaitForSeconds(gemSpeed);
        matchFinder.FindAllMatches();
        if(matchFinder.currentMatches.Count > 0)
        {
            yield return new WaitForSeconds(gemSpeed);
            DestroyMatches();
        }
        else
        {
            yield return new WaitForSeconds(gemSpeed);
            currentState = BoardState.move;
        }
    }

    private void RefillBoard()
    {
        for(int x = 0; x < boardWidth; x++)
        {
            for(int y = 0; y < boardHeight; y++)
            {
                if(allGems[x, y] == null)
                {
                    int gemToUse = Random.Range(0, gemsToUse.Length);
                    SpawnGem(new Vector2Int(x, y), gemsToUse[gemToUse]);
                }
                
            }
        }

        CheckMisplacedGems();
    }

    private void CheckMisplacedGems()
    {
        // создаём лист со всеми гемами в сцене
        List<GemController> foundGems = new List<GemController>();
        foundGems.AddRange(FindObjectsOfType<GemController>());
        // удаляем из листа все зарегестрированные
        for(int x = 0; x < boardWidth; x++)
        {
            for(int y = 0; y < boardHeight; y++)
            {
                if(foundGems.Contains(allGems[x, y]))
                {
                    foundGems.Remove(allGems[x, y]);
                }   
            }
        }
        // если в листе остались гемы - удаляем
        if(foundGems.Count > 0)
        {
            foreach(GemController g in foundGems)
            {
                Destroy(g.gameObject);
            }
        }
        
    }

    private void SpawnExplosionFX(Vector3 posToSpawn)
    {
        GameObject matchFX = Instantiate(matchExplosionPrefab, posToSpawn, Quaternion.identity);
    }

    private IEnumerator SpawnExplosionFXCo(Vector3 posToSpawn)
    {
        Instantiate(matchExplosionPrefab, posToSpawn, Quaternion.identity);
        yield return new WaitForSeconds(gemSpeed);
    }
}
