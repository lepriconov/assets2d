using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Match3BoardLayout : MonoBehaviour
{
    public LayoutRow[] allRows;
    // Start is called before the first frame update
    public GemController[,] GetLayout()
    {
        GemController[,] theLayout = new GemController[allRows[0].gemsInRaw.Length, allRows.Length];

        for(int y = 0; y < allRows.Length; y++)
        {
            for(int x = 0; x < allRows[y].gemsInRaw.Length; x++)
            {
                if(x < theLayout.GetLength(0))
                {
                    if(allRows[y].gemsInRaw[x] != null)
                    {
                        theLayout[x, allRows.Length - 1 - y] = allRows[y].gemsInRaw[x];
                    }
                }
            }
        }
        

        return theLayout;
    }
}

[System.Serializable]
public class LayoutRow
{
    public GemController[] gemsInRaw;
}
