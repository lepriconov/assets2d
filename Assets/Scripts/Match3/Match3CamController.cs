using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Match3CamController : MonoBehaviour
{
    private Match3BoardController boardController;
    // Start is called before the first frame update
    void Start()
    {
        boardController = FindObjectOfType<Match3BoardController>();
        
        transform.position = new Vector3(boardController.boardWidth/2f - .5f, boardController.boardHeight/2f - .5f, -10f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
